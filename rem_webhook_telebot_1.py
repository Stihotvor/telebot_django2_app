import time
import telebot
import logging


API_TOKEN = 'TOKEN'

logger = telebot.logger
telebot.logger.setLevel(logging.INFO)

bot = telebot.TeleBot(API_TOKEN)

# Remove webhook, it fails sometimes the set if there is a previous webhook
bot.remove_webhook()
