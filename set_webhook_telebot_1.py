import time
import telebot
import logging


API_TOKEN = 'TOKEN'

logger = telebot.logger
telebot.logger.setLevel(logging.INFO)

bot = telebot.TeleBot(API_TOKEN)

telebot_1_url = 'TELEBOT_URL'

WEBHOOK_HOST = 'HOST'
WEBHOOK_PORT = 443  # 443, 80, 88 or 8443 (port need to be 'open')
WEBHOOK_LISTEN = 'VPS_IP'  # In some VPS you may need to put here the IP addr

WEBHOOK_URL_BASE = "https://%s:%s" % (WEBHOOK_HOST,WEBHOOK_PORT)
WEBHOOK_URL_PATH = "/%s/" % (telebot_1_url)

# Remove webhook, it fails sometimes the set if there is a previous webhook
bot.remove_webhook()

time.sleep(0.1)

# Set webhook
bot.set_webhook(url=WEBHOOK_URL_BASE+WEBHOOK_URL_PATH)
