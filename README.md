**Telebot app for Django 2.x**

This is a telegram bot app for Django 2.x website. This example has been developed using [pyTelegramBotAPI](https://github.com/eternnoir/pyTelegramBotAPI)

---

## Installation

You’ll need to have already installed and configured Django 2.x on Your server. Also You will need an access to Your server ssl certificates.

1. Install mysqlclient and pyTelegramBotAPI
2. Copy the folder "telebot" to Your Django project folder
3. Copy set_webhook_telebot_1.py and rem_webhook_telebot_1.py to Your web root folder folder (ex. public_html)
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.
